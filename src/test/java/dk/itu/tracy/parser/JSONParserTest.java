package dk.itu.tracy.parser;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import dk.itu.tracy.entity.*;

public class JSONParserTest {
	static File file;
	static String goodJson;
	static final String[] datafiles = { "JekinsParsingConfig.json" };
	List<Entity> result;
//	final JSONParser jp = new JSONParser();
//	static ClassLoader classLoader = JSONParser.class.getClassLoader();
	static String parseConfig;
	
//	@Test
//	public void validateJsonConfigFile() throws ProcessingException, IOException {
//		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
//		final ObjectMapper mapper = new ObjectMapper();
//		
//		ProcessingReport report;
//
//		final File schemaF = new File(classLoader.getResource("JsonParser.schema.json").getFile());
//		final JsonSchema schema = factory.getJsonSchema(mapper.readTree(schemaF));
//
//		for (int i = 0; i < datafiles.length; i++) {
//			JsonNode jsonData = mapper.readTree(new File(classLoader.getResource(datafiles[i]).getFile()));
//			report = schema.validate(jsonData);
//			assertTrue("Json file not passing:"+datafiles[i]+report,report.isSuccess());
//		}
//
//	}
//	@Test
//	public void testParseJenkinsPostWithChanges() throws IOException, ParseException {
//		parseConfig= new String(
//				Files.readAllBytes(new File(classLoader.getResource("JekinsParsingConfig.json").getFile()).toPath()));
//		String jenkinsJson = new String(Files.readAllBytes(
//				new File(classLoader.getResource("Jenkins/jenkinsPostWithChanges.json").getFile()).toPath()));
//		result=jp.parse(parseConfig, jenkinsJson);
//		Entity ent = result.get(0);
//		assertEquals("201", ent.getId());
//		assertArrayEquals(new String[] { "fc81df1fe70f7a3272008278d02cfa8aa4e95b90" }, ent.getPrev());
//		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
//		Date date = df1.parse("2016-03-20T14:18:02+01:00");
//		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
//		assertEquals("Jenkins build started",ent.getType());
//		
//	
//	}
//	@Test
//	public void testParseJenkinsPre() throws IOException, ParseException {
//		parseConfig= new String(
//				Files.readAllBytes(new File(classLoader.getResource("JekinsParsingConfig.json").getFile()).toPath()));
//		String jenkinsJson = new String(
//				Files.readAllBytes(new File(classLoader.getResource("Jenkins/jenkinsPre.json").getFile()).toPath()));
//
//		result=jp.parse(parseConfig, jenkinsJson);
//		Entity ent = result.get(0);
//		assertEquals("195", ent.getId());
//		assertArrayEquals(new String[] {}, ent.getPrev());
//		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
//		Date date = df1.parse("2016-03-20T13:04:43+01:00");
//		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
//		assertEquals("Jenkins build started",ent.getType());
//	}
//	@Test
//	public void testParseJenkinsPostNoChanges() throws IOException, ParseException {
//		parseConfig= new String(
//				Files.readAllBytes(new File(classLoader.getResource("JekinsParsingConfig.json").getFile()).toPath()));
//		String jenkinsJson = new String(Files.readAllBytes(
//				new File(classLoader.getResource("Jenkins/jenkinsPost.json").getFile()).toPath()));
//		result=jp.parse(parseConfig, jenkinsJson);
//		Entity ent = result.get(0);
//		assertEquals("195", ent.getId());
//		assertArrayEquals(new String[] {}, ent.getPrev());
//		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
//		Date date = df1.parse("2016-03-20T13:04:43+01:00");
//		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
//		assertEquals("Jenkins build started",ent.getType());
//		
//	
//	}

}
