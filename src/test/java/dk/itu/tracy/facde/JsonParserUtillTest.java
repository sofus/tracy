package dk.itu.tracy.facde;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import dk.itu.tracy.entity.Entity;

public class JsonParserUtillTest {
	static File file;
	static String goodJson;
	static final ClassLoader classLoader = JsonParserUtillTest.class.getClassLoader();
	static final String[] datafiles = { "entity/testWithStringData.json", "entity/testWithArrayData.json", "entity/testWithObjectData.json","entity/testGitLabCommit.json" };
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void validateJsonFiles() throws ProcessingException, IOException {
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final ObjectMapper mapper = new ObjectMapper();
		
		ProcessingReport report;

		final File schemaF = new File(classLoader.getResource("protocol.schema.json").getFile());
		final JsonSchema schema = factory.getJsonSchema(mapper.readTree(schemaF));

		for (int i = 0; i < datafiles.length; i++) {
			JsonNode jsonData = mapper.readTree(new File(classLoader.getResource(datafiles[i]).getFile()));
			report = schema.validate(jsonData);
			assertTrue("Json file not passing:"+datafiles[i]+report,report.isSuccess());
		}

	}

	@Test
	public void testParseStringJson() throws ParseException, IOException {
		file = new File(classLoader.getResource("entity/testWithStringData.json").getFile());
		goodJson = new String(Files.readAllBytes(file.toPath()));
		Entity ent = JsonParserUtill.parseJson(goodJson);
		assertEquals("1234", ent.getId());
		assertArrayEquals(new String[] { "4321" }, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-17T13:11:05+01");
		assertEquals(date, ent.getTimestamp());
		assertEquals("codecommit",ent.getType());
		assertEquals("run=yes", ent.getData().getAsString());
	}
	@Test
	public void testParseArrayJson() throws ParseException, IOException {
		file = new File(classLoader.getResource("entity/testWithArrayData.json").getFile());
		goodJson = new String(Files.readAllBytes(file.toPath()));
		Entity ent = JsonParserUtill.parseJson(goodJson);
		assertEquals("12345", ent.getId());
		assertArrayEquals(new String[] { "4321" }, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-17T13:11:05+01");
		assertEquals(date, ent.getTimestamp());
		assertEquals("codecommit",ent.getType());
		assertEquals("yes", ent.getData().getAsJsonArray().get(0).getAsJsonObject().get("output").getAsString());
	}
	@Test
	public void testParseObjectJson() throws ParseException, IOException {
		file = new File(classLoader.getResource("entity/testWithObjectData.json").getFile());
		goodJson = new String(Files.readAllBytes(file.toPath()));
		Entity ent = JsonParserUtill.parseJson(goodJson);
		assertEquals("4321", ent.getId());
		assertArrayEquals(new String[] { "1234" }, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-17T13:11:05+01");
		assertEquals(date, ent.getTimestamp());
		assertEquals("codecommit",ent.getType());
		assertEquals("success", ent.getData().getAsJsonObject().get("output").getAsString());
	}
	@Test
	public void testParseGitlabJson() throws ParseException, IOException {
		file = new File(classLoader.getResource("entity/testGitLabCommit.json").getFile());
		goodJson = new String(Files.readAllBytes(file.toPath()));
		Entity ent = JsonParserUtill.parseJson(goodJson);
		assertEquals("b9dedf94faf7c2df08ee5359e7793de99149dba9", ent.getId());
		assertArrayEquals(new String[] { "PROJ-5" }, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-02-17T16:13:04+00:00");
		assertEquals(date, ent.getTimestamp());
		assertEquals("code-commit",ent.getType());
		assertEquals("john", ent.getData().getAsJsonObject().get("repository").getAsJsonObject().get("name").getAsString());
	}
	private void validateJson(String[] json) throws JsonProcessingException, ProcessingException, IOException{
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final ObjectMapper mapper = new ObjectMapper();
		
		ProcessingReport report;

		final File schemaF = new File(classLoader.getResource("protocol.schema.json").getFile());
		final JsonSchema schema = factory.getJsonSchema(mapper.readTree(schemaF));

		for (int i = 0; i < json.length; i++) {
			JsonNode jsonData = mapper.readTree(json[i]);
			report = schema.validate(jsonData);
			assertTrue("Json file not passing:"+json[i]+report,report.isSuccess());
		}
	}
	@Test
	public void testEntityToJson() throws JsonProcessingException, ProcessingException, IOException{
		String[] prevId = {"2"};
		Entity e1 = new Entity();
		e1.setData("data");
		e1.setId("id");
		e1.setPrev(prevId);
		e1.setTimestamp(new Date());
		e1.setType("type");
		e1.setHealthy(true);
		e1.setUrl("www.dr.dk");
		System.out.println(e1);
		String[] json= new String[1];
		json[0]=JsonParserUtill.parseEntity(e1);
		validateJson(json);
	}
//	@Test
//	public void testJsonToMap() throws IOException{
//		file = new File(classLoader.getResource("entity/testGitLabCommit.json").getFile());
//		goodJson = new String(Files.readAllBytes(file.toPath()));
//		Entity ent = JsonParserUtill.parseJson(goodJson);
//		Map<String,Object> map=	JsonParserUtill.jsonToMap(ent.getData());
//		System.out.println(map.keySet());
//	}
	
	
}
