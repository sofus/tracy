package dk.itu.tracy.facde;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import dk.itu.tracy.dsl.GroovyScriptEngineTest;
import dk.itu.tracy.entity.Entity;

public class Neo4JFacadeTest {
	ClassLoader cl = GroovyScriptEngineTest.class.getClassLoader();
	Properties prop = new Properties();
	Neo4JFacade facade;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		FileReader fr = new FileReader(cl.getResource("NEO4J.properties").getFile());
		prop.load(fr);
//		facade = new Neo4JFacade(prop);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMergeShadow() {
		String[] prevId = {"2"};
		Entity e1 = new Entity();
		e1.setData("data");
		e1.setId("1");
		e1.setPrev(prevId);
		e1.setTimestamp(new Date());
		e1.setType("type");
		e1.setUrl("www.dr.dk");
		String[] prevId2={};
		Entity e2 = new Entity();
		e2.setData("data");
		e2.setId("2");
		e2.setPrev(prevId2);
		e2.setTimestamp(new Date());
		e2.setType("type");
		e2.setUrl("www.dr.dk");
		System.out.println(prop.toString());
		Entity e3 = new Entity();
		JsonArray ja = new JsonArray();
		
		ja.add(new JsonPrimitive("one"));
		ja.add(new JsonPrimitive("two"));
		ja.add(new JsonPrimitive("three"));
		
		e3.setData(ja);
		e3.setId("3");
		e3.setPrev(prevId2);
		e3.setTimestamp(new Date());
		e3.setType("type");
		e3.setUrl("www.dr.dk");
		System.out.println(prop.toString());
//		
//		facade.persistEntity(e1);
//		facade.persistEntity(e2);
//		facade.persistEntity(e3);
//		
		
	}
	@Test
	public void testPersistence() throws IOException{
		String jsonString = new String(Files.readAllBytes(new File(cl.getResource("entity/testGitLabCommit.json").getFile()).toPath()));
		Entity e =JsonParserUtill.parseJson(jsonString);
//		facade.persistEntity(e);
	}
	

}
