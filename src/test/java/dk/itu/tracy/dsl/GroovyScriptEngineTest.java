package dk.itu.tracy.dsl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import dk.itu.tracy.entity.Entity;
import dk.itu.tracy.facde.JsonParserUtill;

public class GroovyScriptEngineTest {
	static final String[] jenkinsDatafiles = { "Jenkins/jenkinsPost.json", "Jenkins/jenkinsPostWithChanges.json",
			"Jenkins/jenkinsPre.json" };
	static final String[] gitLabDatafiles = { "Gitlab/GitlabPushWebHook.json", "Gitlab/GitlabPushWebHook2.json" };
	static final String[] jiraDataFiles={"Jira/JiraTask7IssueSolvedWithGitCommit.json","Jira/JiraTask7SetInProgress.json","Jira/JiraTask7Created.json"};
	static final String jiraDSL = "dsl/jira.dsl";
	static final String gitLabDSL = "dsl/gitlab.dsl";
	static final String jenkinsDSL = "dsl/jenkins.dsl";
	final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
	final ObjectMapper mapper = new ObjectMapper();
	ClassLoader cl = GroovyScriptEngineTest.class.getClassLoader();
	final File schemaF = new File(cl.getResource("protocol.schema.json").getFile());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJenkinsDSLParser() throws JsonProcessingException, ProcessingException, IOException {
		String dslFile = new String(Files.readAllBytes(new File(cl.getResource(jenkinsDSL).getFile()).toPath()));
		for (int i = 0; i < jenkinsDatafiles.length; i++) {
			String jsonString = new String(Files.readAllBytes(new File(cl.getResource(jenkinsDatafiles[i]).getFile()).toPath()));
			testFiles(dslFile, jsonString, jenkinsDatafiles[i]);
		}

	}

	@Test
	public void testGitlabParser() throws Exception {
		String dslFile =  new String(Files.readAllBytes(new File(cl.getResource(gitLabDSL).getFile()).toPath()));
		for (int i = 0; i < gitLabDatafiles.length; i++) {
			String jsonString = new String(Files.readAllBytes(new File(cl.getResource(gitLabDatafiles[i]).getFile()).toPath()));
			testFiles(dslFile, jsonString, gitLabDatafiles[i]);
		}
	}
	@Test
	public void testJiraParser() throws Exception {
		String dslFile = new String(Files.readAllBytes(new File(cl.getResource(jiraDSL).getFile()).toPath()));
		for (int i = 0; i < jiraDataFiles.length; i++) {
			String jsonString =new String(Files.readAllBytes(new File(cl.getResource(jiraDataFiles[i]).getFile()).toPath()));
			testFiles(dslFile, jsonString, jiraDataFiles[i]);
		}
	}
	/**
	 * Helper method for testing DSL's and inputfiles together.
	 * @param dsl
	 * @param input
	 * @param filename
	 * @throws JsonProcessingException
	 * @throws ProcessingException
	 * @throws IOException
	 */
	public void testFiles(String dsl, String input, String filename)
			throws JsonProcessingException, ProcessingException, IOException {

		ProcessingReport report;
		final JsonSchema schema = factory.getJsonSchema(mapper.readTree(schemaF));
		System.out.println("Running file:"+filename);
		List<Entity> ents = GroovyScriptEngine.runDSL(dsl, input);
		for (Entity entity : ents) {
			report = schema.validate(mapper.readTree(JsonParserUtill.parseEntity(entity)));
			assertTrue("Json file not passing:" + filename + report, report.isSuccess());

		}
	}
	@Test
	public void testParseJenkinsPostWithChanges() throws IOException, ParseException {
		String jenkinsJson = new String(Files.readAllBytes(
				new File(cl.getResource("Jenkins/jenkinsPostWithChanges.json").getFile()).toPath()));
		String dsl = new String(Files.readAllBytes(
				new File(cl.getResource(jenkinsDSL).getFile()).toPath()));
		
		List<Entity> ents =GroovyScriptEngine.runDSL(dsl, jenkinsJson);
		Entity ent = ents.get(0);
		assertEquals("Test-job #201", ent.getId());
		assertArrayEquals(new String[] { "fc81df1fe70f7a3272008278d02cfa8aa4e95b90" }, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-20T14:18:02+01:00");
		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
		assertEquals("Jenkins building started",ent.getType());
		
	
	}
	@Test
	public void testParseJenkinsPre() throws IOException, ParseException {
		String jenkinsJson = new String(Files.readAllBytes(
				new File(cl.getResource("Jenkins/jenkinsPre.json").getFile()).toPath()));
		String dsl = new String(Files.readAllBytes(
				new File(cl.getResource(jenkinsDSL).getFile()).toPath()));
		
		List<Entity> ents =GroovyScriptEngine.runDSL(dsl, jenkinsJson);
		Entity ent = ents.get(0);
		assertEquals("Test-job #195", ent.getId());
		assertArrayEquals(new String[] {}, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-20T13:04:43+01:00");
		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
		assertEquals("Jenkins building started",ent.getType());
	}
	@Test
	public void testParseJenkinsPostNoChanges() throws IOException, ParseException {
		String jenkinsJson = new String(Files.readAllBytes(
				new File(cl.getResource("Jenkins/jenkinsPost.json").getFile()).toPath()));
		String dsl = new String(Files.readAllBytes(
				new File(cl.getResource(jenkinsDSL).getFile()).toPath()));
		
		
		List<Entity> ents =GroovyScriptEngine.runDSL(dsl, jenkinsJson);
		Entity ent = ents.get(0);
		assertEquals("Test-job #195", ent.getId());
		assertArrayEquals(new String[] {}, ent.getPrev());
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date date = df1.parse("2016-03-20T13:04:43+01:00");
		assertEquals(date.getTime()/1000, ent.getTimestamp().getTime()/1000);
		assertEquals("Jenkins building stopped",ent.getType());
		
	
	}
}
