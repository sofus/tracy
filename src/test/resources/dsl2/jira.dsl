//input 'jenkinsPostWithChanges.json'
parser 'JSON'
entity {
    id = eval('$.issue.id')
    prev = array(retrievePrev(),eval('$.issue.id'))
    url = eval('$.issue.self')
    type = eval('$.webhookEvent')
    healthy=true
    data = all
    timestamp = new Date(eval('$.timestamp', 'java.lang.Long'))
}


def retrievePrev(){
String issuetrack = eval('$.issue.fields.comment.comments[0].body')
if (issuetrack!=null)
return issuetrack.substring(issuetrack.indexOf('[')+1,issuetrack.indexOf('|'))
return null
}
