parser 'JSON'

list = eval '$.commits[*].id','List'
list.eachWithIndex { val, idx ->
entity {
    id = eval('$.commits['+idx+'].id')
    prev = ""
    url = eval('$.commits['+idx+'].url')
    type = 'commit'
    healthy=true
    data = all
    timestamp = parseDate(eval('$.commits['+idx+'].timestamp'),"yyyy-MM-dd'T'HH:mm:ssX")
}
}

//println eval('$.user_id','long')
entity {
    id = eval('$.before')+eval('$.after')
    prev = eval('$.commits[*].id','List') 
    url = eval('$.repository.homepage')
    type = eval('$.object_kind')
    healthy=true
    data = all
    timestamp = new Date()
}
