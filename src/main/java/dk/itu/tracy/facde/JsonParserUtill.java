/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.itu.tracy.facde;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import dk.itu.tracy.entity.Entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sofus
 */
public class JsonParserUtill {
	static final Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();

	public static Entity parseJson(String jsonString) {
		Entity ent = new Entity();
		JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
		ent.setId(json.get("id").getAsString());
		JsonArray ja = json.get("prev").getAsJsonArray();
		String[] prev = new String[ja.size()];
		int i = 0;
		for (JsonElement jsonElement : ja) {
			prev[i++] = jsonElement.getAsString();
		}
		ent.setPrev(prev);
		ent.setType(json.get("type").getAsString());
		ent.setData(json.get("data"));
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		try {
			ent.setTimestamp(df1.parse(json.get("timestamp").getAsString()));
			ent.setUrl((json.get("url").getAsString()));
		} catch (ParseException ex) {
			Logger.getLogger(JsonParserUtill.class.getName()).log(Level.SEVERE, null, ex);
		}
		return ent;
	}

	public static String parseEntity(Entity ent) {
		return gson.toJson(ent);
	}
	/**
	 * Only usefull that day when NEO4J can have nested maps into one node
	 * @param je
	 * @return
	 */
	public static Map<String,Object> jsonToMap(JsonElement je){
		Map<String,Object> map = new HashMap<String,Object>();
		map = (Map<String,Object>) gson.fromJson(je, map.getClass());
		return map;
	}
}
