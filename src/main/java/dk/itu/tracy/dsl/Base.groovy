package dk.itu.tracy.dsl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser

import dk.itu.tracy.entity.Entity
import dk.itu.tracy.facde.AMQPFacade;

import java.awt.im.InputContext;
import java.text.DateFormat
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date
import java.util.Properties
import dk.itu.tracy.dsl.JsonPathParser

import org.apache.commons.lang.Entities;;;


public abstract class Base extends Script {
	List<Entity> entities = new ArrayList<Entity>()
    Parser parser
    ClassLoader cl = Base.class.getClassLoader()
	
    Entity entity(Closure myClosure){
		Entity myConfig = new Entity();
		ContextHelper.executeInContext(myClosure, myConfig)
		entities.add(myConfig)
		this.binding.entities=entities//sends the 
		return myConfig
	}
	def JsonObject getAll(){
		new JsonParser().parse(parser.toJson(this.binding.input)).getAsJsonObject();
	}
	String[] array(String... args){
		//removing nulls
		args = args - null
		return args;
	}
    def eval(String key){
        return parser.eval(key)
    }
	def eval(String key,String returntype){
		switch (returntype.toLowerCase())
		{	//to ease the expressibility of the DSL code
			case 'list':returntype='java.util.List';break;
			case 'string':returntype='java.lang.String';break;
			case 'long':returntype='java.lang.Long';break;
			case 'boolean':returntype='java.lang.Boolean';break;
			default:break;
		}
		return parser.eval(key,returntype)
	}

	Date parseDate(String date, String format) throws ParseException {
		final DateFormat df = new SimpleDateFormat(format)
		df.parse(date)
	}
    void parser(String name){
		switch(name.toLowerCase())
		{
			case 'json':parser = new JsonPathParser()
			parser.setup(this.binding.input)
			break
			default:  println "Parser $name not supported!"
            System.exit(1)
		}
        if(name.equalsIgnoreCase('json')){
            parser = new JsonPathParser()
			parser.setup(this.binding.input)
			}
        else{
            println "Parser $name not supported!"
            System.exit(1)
        }
    }
}
