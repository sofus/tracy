package dk.itu.tracy.dsl
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;
import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath
import com.jayway.jsonpath.Option;


class JsonPathParser implements Parser{
	DocumentContext dc;

    @Override
    String eval(String key) {
      dc.read(key,String.class)
    }
	
    public <T> T eval(String key, String clazz) {
    	dc.read(key,Class.forName(clazz))
    }

	void setup(String input) {
		//Supress exeptions gives you a null value when you parse into a invalid path
		Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
		dc = JsonPath.using(conf).parse(input);
	}

	@Override
	public String toJson(String input) {
		input
	}
	

}
