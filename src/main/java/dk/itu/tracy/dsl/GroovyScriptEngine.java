package dk.itu.tracy.dsl;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;

import dk.itu.tracy.entity.Entity;

import java.util.List;

public class GroovyScriptEngine {
    public static List<Entity> runDSL(String dslFile,String input){
    	// Prepare shell
        CompilerConfiguration config = new CompilerConfiguration();
        config.setScriptBaseClass("dk.itu.tracy.dsl.Base");
        Binding binding = new Binding();
        binding.setProperty("input", input);
        
        GroovyShell shell = new GroovyShell(binding,config);
        
        
        try {
			shell.evaluate(dslFile);
		} catch (CompilationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (List<Entity>) shell.getProperty("entities");
    	
    }
}
