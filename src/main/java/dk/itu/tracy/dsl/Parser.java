package dk.itu.tracy.dsl;
public interface Parser {
    String eval(String key);
    public <T> T eval(String key,String clazz);
    void setup(String input);
    String toJson(String input);
}
