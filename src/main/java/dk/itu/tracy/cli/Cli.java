package dk.itu.tracy.cli;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;

import dk.itu.tracy.dsl.GroovyScriptEngine;
import dk.itu.tracy.entity.Entity;
import dk.itu.tracy.facde.AMQPFacade;

public class Cli {

	public static void main(String[] args) throws IOException {
		String inputString;
		String dslFile;
		Properties prop = new Properties();
		if (args.length < 3) {
			System.out.println("Error: need arguments [-f] amqp.properties input dsl");
			System.exit(2);
		}
		if (args[0].toLowerCase().equals("-f")) {//Parsed in as paths to the files
			FileReader fr = new FileReader(args[1]);
			prop.load(fr);
			inputString =  new String(Files.readAllBytes(new File(args[2]).toPath()));
			dslFile = new String(Files.readAllBytes(new File(args[3]).toPath()));
		}
		else{//Parsed in as normal strings
			prop.load(new StringReader(args[0]));
			inputString=args[1];
			dslFile=args[2];
		}
		AMQPFacade facade = new AMQPFacade(prop);
		List<Entity> ents = GroovyScriptEngine.runDSL(dslFile, inputString);
		System.out.println("msg parsed");
		for (Entity entity : ents) {
			facade.sendEvent(entity);
		}
		System.out.println("msg sent");
		facade.close();
		System.exit(0);
	}

}
